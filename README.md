# Digital Theatre Video.js theme 🎭

A repo for Digital Theatre's Video.js theme.

![vjs-theme-dt-screenshot](https://i.ibb.co/1vfjVTt/vjs-theme-dt-screenshot.png)

## Usage

If you're using CSS modules in JavaScript, you can install the NPM module:

`npm install --save video.js @digitaltheatre/videojs-theme-dt`

Then just import the files as you would other CSS.

``` javascript
import 'video.js/dist/video-js.css';

import '@digitaltheatre/videojs-theme-dt/dist/theme/index.css';
```

Once you've got the theme pulled in, you can then add the class to your player:

`<video id="my-player" class="video-js vjs-theme-dt" ...>`

## Building these locally

If you want to use the CSS in this repo directly instead of using one of the CDNs or the NPM-hosted version, or just do some development, you'll need to clone the repo and `build` them.

``` bash
git clone https://gitlab.com/digital-theatre/videojs-theme-dt videojs-theme-dt
cd videojs-theme-dt
yarn install
yarn run build
```

This will create a `dist` folder with the post-processed CSS in them, which you can upload right to your site. If you want to develop against these locally, you can run `yarn run dev` to get a local server running a demo of the theme on it.
